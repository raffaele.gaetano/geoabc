import numpy as np
from itertools import chain, product, combinations_with_replacement, permutations
from skimage.segmentation import find_boundaries
from skimage.morphology import dilation,disk,binary_opening
import matplotlib.pyplot as plt
import gdal
import glob
import os
import csv
import ogr
import osr
import datetime
import subprocess

def getCombinations(groups,length,min_changes=2,min_resilience=1):
    out = set()
    for x in combinations_with_replacement(groups,length):
        for p in permutations(x):
            s = 0
            for c in range(len(p)-1):
                if p[c+1] != p[c]:
                    s += 1
            if s >= min_changes:
                out.add(p)
    out_out = set()
    if min_resilience > 1:
        for p in out:
            q = np.pad(np.array(p),min_resilience-1,mode='edge')
            dq = np.abs(np.diff(q))
            r = []
            for i in range(len(dq)-min_resilience+1):
                r.append(np.argmax(np.bincount(dq[i:i+min_resilience])))
            if np.sum(np.array(r)) == 0:
                out_out.add(p)
    else:
        out_out = out
    return np.array([list(c) for c in out_out])


def loadLCMaps(fld,ptrn,mask=None,class_erosion=None):
    lst = sorted(glob.glob(os.path.join(fld,ptrn)))
    ds = gdal.Open(lst[0])
    R,C = ds.RasterYSize, ds.RasterXSize
    ds = None
    msk = None
    if mask is not None:
        mds = gdal.Open(mask)
        msk = mds.ReadAsArray()
        mds = None

    datacube = np.empty((len(lst),R,C))
    for i in range(len(lst)):
        ds = gdal.Open(lst[i])
        datacube[i] = ds.ReadAsArray()
        if msk is not None:
            datacube[i] = np.multiply(datacube[i],msk)
        ds = None
    out = np.rollaxis(datacube,0,3)
    out[out<0] = 0
    if class_erosion is not None:
        for i in range(out.shape[2]):
            se = disk(class_erosion)
            tmp = 1-dilation(find_boundaries(out[:,:,i]),se)
            out[:,:,i] = np.multiply(out[:,:,i],tmp)

    return out.astype(np.int)

def loadConfidenceMaps(fld,ptrn):
    lst = sorted(glob.glob(os.path.join(fld,ptrn)))
    ds = gdal.Open(lst[0])
    R,C = ds.RasterYSize, ds.RasterXSize
    ds = None
    datacube = np.empty((len(lst),R,C))
    for i in range(len(lst)):
        ds = gdal.Open(lst[i])
        datacube[i] = ds.ReadAsArray()
        ds = None
    out = np.rollaxis(datacube,0,3)

    return out.astype(np.float)

def createMapping(dct,max):
    lut = np.zeros(max+1)
    lut[dct.keys()] = dct.values()
    return lut

def findPatterns(lc_maps, time_window_size, group_dict, min_changes=2, min_resilience=1):
    cmb = getCombinations(set(group_dict.values()),time_window_size,min_changes,min_resilience)
    lut = createMapping(group_dict,int(lc_maps.max()+1))
    out = []
    for t in range(lc_maps.shape[2]-time_window_size+1):
        tmp = lut[lc_maps[:,:,t:t+time_window_size].astype(np.int)]
        ptrn = np.zeros((tmp.shape[0],tmp.shape[1]))
        k = 1
        for c in cmb:
            tmpc = np.tile(c,(tmp.shape[0],tmp.shape[1],1))
            comp = np.all(tmp==tmpc,axis=2)
            #comp2 = np.dstack((ptrn, comp))
            #ptrn = np.any(comp2,axis=2)
            ptrn += k * comp
            k += 1
            print 'Completed combination ' + str(c) + ' for period ' + str(t + 1) + ' of ' + str(lc_maps.shape[2] - time_window_size + 1) + '...'
        out.append(ptrn)
    print 'Finished!'
    return out,cmb

def getAreas(ptrns,cmb,show=False):
    ar = np.zeros((len(cmb),len(ptrns)))
    for c in range(len(cmb)):
        for p in range(len(ptrns)):
            ar[c,p] = np.sum(ptrns[p]==(c+1))
    if show:
        for i in range(len(ar)):
            plt.figure()
            plt.plot(ar[i])
            plt.title(cmb[i])
    return ar

def PANGAEA_csv2shp(fn,out):

    classcode = {'Cerrado':1,'Fallow_Cotton':2,'Forest':3,'Pasture':4,'Soy_Corn':5,'Soy_Cotton':6,'Soy_Fallow':7,'Soy_Millet':8,'Soy_Sunflower':9,'Sugarcane':10,'Urban Area':11,'Water':12}

    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)
    drv = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists(out):
        drv.DeleteDataSource(out)
    ds = drv.CreateDataSource(out)
    ly = ds.CreateLayer(out, srs, ogr.wkbPoint)

    f_id = ogr.FieldDefn('ID', ogr.OFTInteger)
    f_stdate = ogr.FieldDefn('StartDate', ogr.OFTString)
    f_stdate.SetWidth(20)
    f_endate = ogr.FieldDefn('EndDate', ogr.OFTString)
    f_endate.SetWidth(20)
    f_class = ogr.FieldDefn('Class', ogr.OFTString)
    f_class.SetWidth(20)
    f_code = ogr.FieldDefn('Code', ogr.OFTInteger)

    ly.CreateField(f_id)
    ly.CreateField(f_stdate)
    ly.CreateField(f_endate)
    ly.CreateField(f_class)
    ly.CreateField(f_code)

    intervals = set()

    header = True
    with open(fn) as gtf:
        rdr = csv.reader(gtf, delimiter=',')
        for row in rdr:
            if header:
                header = False
                continue

            point = ogr.Geometry(ogr.wkbPoint)
            point.AddPoint(float(row[1]),float(row[2]))

            feat = ogr.Feature(ly.GetLayerDefn())
            feat.SetGeometry(point)
            feat.SetField('ID',int(row[0]))
            feat.SetField('StartDate', datetime.datetime.strptime(row[3],'%Y-%m-%d').strftime('%Y%m%d'))
            feat.SetField('EndDate', datetime.datetime.strptime(row[4], '%Y-%m-%d').strftime('%Y%m%d'))
            feat.SetField('Class',row[5])
            feat.SetField('Code', classcode[row[5]])

            ly.CreateFeature(feat)

            intervals.add((datetime.datetime.strptime(row[3], '%Y-%m-%d').strftime('%Y%m%d'), datetime.datetime.strptime(row[4], '%Y-%m-%d').strftime('%Y%m%d')))

    ds = None
    return intervals

def getDatesFromFiles(fld,
                      template='MOD13Q1.006__250m_16_days_NDVI_',
                      date_pos=34,
                      date_len=7,
                      date_jul=True,
                      ext='tif'):

    lst = sorted(glob.glob(fld + '/' + template + '*.' + ext))
    out_lst = []

    with open(fld + '/dates.txt', 'wb') as df:
        for f in lst:
            dt = os.path.basename(f)[date_pos:date_pos + date_len]
            if date_jul:
                y, jd = int(dt[0:4]), int(dt[4:7])
                dt = (datetime.datetime(y, 1, 1) + datetime.timedelta(jd - 1)).strftime('%Y%m%d')
            out_lst.append(dt)
            df.write(dt + '\n')

    return out_lst

def getIntervalIndices(dates,start_date,end_date,date_fmt='%Y%m%d'):
    sd = datetime.datetime.strptime(start_date, date_fmt)
    ed = datetime.datetime.strptime(end_date, date_fmt)
    td1 = datetime.datetime.strptime(dates[-1],'%Y%m%d') - datetime.datetime.strptime(dates[0],'%Y%m%d')
    td2 = datetime.datetime.strptime(dates[-1], '%Y%m%d') - datetime.datetime.strptime(dates[0], '%Y%m%d')
    id1 = None
    id2 = None
    for i in range(len(dates)):
        if (sd - datetime.datetime.strptime(dates[i],'%Y%m%d')) >= datetime.timedelta(0) and (sd - datetime.datetime.strptime(dates[i],'%Y%m%d')) < td1:
            id1 = i
            td1 = sd - datetime.datetime.strptime(dates[i],'%Y%m%d')
        if (ed - datetime.datetime.strptime(dates[i],'%Y%m%d')) >= datetime.timedelta(0) and (ed - datetime.datetime.strptime(dates[i],'%Y%m%d')) < td2:
            id2 = i
            td2 = ed - datetime.datetime.strptime(dates[i],'%Y%m%d')

    #if td2 > datetime.timedelta(0) and id2 < (len(dates)-1):
    #    id2 += 1

    return id1,id2


def buildSeasonalVRTs(fld,intervals,prod=['red','NIR','MIR','NDVI','EVI'],template='MYD13Q1.006__250m_16_days_NDVI_',ext='tif'):
    dt = getDatesFromFiles(fld,template=template)
    lengths = []
    fd,ld = datetime.datetime.strptime(dt[0],'%Y%m%d'),datetime.datetime.strptime(dt[-1],'%Y%m%d')
    opt = gdal.BuildVRTOptions(separate=True)
    for it in intervals:
        d1,d2 = datetime.datetime.strptime(it[0],'%Y%m%d'),datetime.datetime.strptime(it[1],'%Y%m%d')
        c1 = (d1 - fd) >= datetime.timedelta(0)
        c2 = (ld - d2) >= datetime.timedelta(0)
        if c1 and c2:
            flst = []
            for p in prod:
                lst = sorted(glob.glob(fld + os.sep + '*' + p + '*.' + ext))
                i1,i2 = getIntervalIndices(dt,it[0],it[1])
                flst.extend(lst[i1:i2+1])
            outfn = fld + os.sep + 'STACK_' + it[0] + '-' + it[1] + '.vrt'
            ds = gdal.BuildVRT(outfn,flst,options=opt)
            ds = None
            lengths.append(len(flst))

    return lengths


def computeTrainingFeatures(trn,fld,template='STACK_',nodatapos=(0,0)):
    vds = ogr.Open(trn,1)
    ly = vds.GetLayer(0)
    feat = []

    tst = gdal.Open(glob.glob(fld + os.sep + template + '*.vrt')[0])
    nodataarr = tst.ReadAsArray(nodatapos[0],nodatapos[1],1,1).flatten().astype(np.float)
    print "No Data array : " + str(nodataarr)

    for i in range(len(nodataarr)):
        ly.CreateField(ogr.FieldDefn('v' + str(i).zfill(4),ogr.OFTReal))

    cnt = 0
    for f in ly:
        g = f.GetGeometryRef()
        x,y = g.GetX(),g.GetY()
        rfn = fld + os.sep +template + f.GetField('StartDate') + '-' + f.GetField('EndDate') + '.vrt'
        if os.path.exists(rfn):
            rds = gdal.Open(rfn)
            gt = rds.GetGeoTransform()
            xr,yr = int((x-gt[0])/gt[1]),int((y-gt[3])/gt[5])
            ar = rds.ReadAsArray(xr,yr,1,1).flatten().astype(np.float)
            rds = None
        else:
            ar = nodataarr
        feat.append(ar)

        for i in range(len(ar)):
            f.SetField('v' + str(i).zfill(4),ar[i])
            ly.SetFeature(f)

        cnt += 1
        if not (cnt%10):
            print 'Written ' + str(cnt) + ' features...'


    vds = None
    np.save('feat',feat)

    return feat

def PANGAEA_classify(fld,mfn,ofld,mskfn=None):
    lst = glob.glob(fld + os.sep + 'STACK_*.vrt')
    for f in lst:
        cmd = ['otbcli_ImageClassifier','-in',f,'-model',mfn]
        if mskfn is not None and os.path.exists(mskfn):
            cmd += ['-mask',mskfn]
        cmd += ['-nodatalabel','0','-out',ofld + os.sep + os.path.basename(f).replace('.vrt','_CLASSIF.tif'),'uint8','-confmap',ofld + os.sep + os.path.basename(f).replace('.vrt','_CONFMAP.tif')]
        subprocess.call(cmd,shell=True)
    return

def PANGAEA_postproc(fld,msk):
    lst = sorted(glob.glob(fld + os.sep + 'STACK_*_CLASSIF.tif'))
    for f in lst:
        fo = f.replace('.tif','_FINAL.tif')
        cmd = ['otbcli_BandMathX','-il',f,msk,'-exp','im1b1*im2b1','-out',fo, 'uint8']
        subprocess.call(cmd,shell=True)
        ds = gdal.Open(fo,1)
        ds.GetRasterBand(1).SetNoDataValue(0)
        ds = None
        cf = f.replace('_CLASSIF','_CONFMAP')
        cfo = cf.replace('.tif','_FINAL.tif')
        cmd = ['otbcli_BandMathX', '-il', cf, msk, '-exp', 'im1b1*im2b1', '-out', cfo]
        subprocess.call(cmd, shell=True)
    return

def processPatterns(ptrns,rad=1):
    pout = []
    for p in ptrns:
        pout.append(np.multiply(p,binary_opening(p>0,disk(rad))))
    return pout

def createGeoReference(arr,ref,fn):
    ds = gdal.Open(ref)
    drv = gdal.GetDriverByName('GTiff')
    if len(arr.shape) == 2:
        B = 1
    elif len(arr.shape) == 3:
        B = arr.shape[2]
    else:
        return -1
    R,C = arr.shape[0],arr.shape[1]
    ds_out = drv.Create(fn,C,R,B,gdal.GDT_Float32)
    ds_out.SetGeoTransform(ds.GetGeoTransform())
    ds_out.SetProjection(ds.GetProjection())
    if B == 1:
        ds_out.GetRasterBand(1).WriteArray(arr)
    else:
        for i in range(1,B+1):
            ds_out.GetRasterBand(i).WriteArray(arr[:,:,i-1])

    ds_out = None
    ds = None

    return 0


