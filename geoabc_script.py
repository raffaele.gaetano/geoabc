from geoabc import *

# Dictionnaire pour regrouper des classes
dct = {1:4,2:2}
# avant on faisait (version camara):
# dct = {1:4,2:5,2:6,2:7,2:8}

# Chercher combinaisons de classes sur periodes
cmb = getCombinations(dct,5)
# attention: remplacer '\' avec '/'

# Charger cartes et confiances
lcm = loadLCMaps('D:/TEST/MatoGrosso/ST/camara/Maps','*_CLASSIF_FINAL.tif',mask='D:/TEST/MatoGrosso/ST/camara/MatoGrossoCropOp3.tif')
confmaps = loadConfidenceMaps('D:/TEST/MatoGrosso/ST/camara/Maps','*_CONFMAP_FINAL.tif')

# Trouver les patterns sur les cartes (on passe cartes et dictionnaires)
# retourne patterns et combinaisons
pts, cmb = findPatterns(lcm,6,dct,min_resilience=2)

# Calcul des surfaces par pattern
areas = getAreas(pts,cmb,show=True)

# Georeferencer un array python
createGeoReference(pts[0],'D:/TEST/MatoGrosso/ST/camara/MatoGrossoMask.tif','D:/TEST/MatoGrosso/ST/camara/Pattern0.tif')

###### CARTES OC SOL MODIS ######
intervalles = PANGAEA_csv2shp('D:/TEST/MatoGrosso/ST/camara/training_dataset_v2.csv','D:/TEST/MatoGrosso/ST/camara/test/training_dataset_v2.shp')
lens = buildSeasonalVRTs('G:/DATA/MODIS',intervalles)
feat = computeTrainingFeatures('D:/TEST/MatoGrosso/ST/camara/test/training_dataset_v2.shp','G:/DATA/MODIS')

# ATTENTION : TRAITER LE SHAPEFILE COMME ON LE SOUHAITE
# à faire avec otb
'''
otbcli_TrainVectorClassifier -io.vd training_dataset_v2_work.shp -io.out RF_Full.model -io.confmatout RF_Full.confmat.csv -cfield Code0 -feat v0000 v0001 v0002 v0003 v0004 v0005 v0006 v0007 v0008 v0009 v0010 v0011 v0012 v0013 v0014 v0015 v0016 v0017 v0018 v0019 v0020 v0021 v0022 v0023 v0024 v0025 v0026 v0027 v0028 v0029 v0030 v0031 v0032 v0033 v0034 v0035 v0036 v0037 v0038 v0039 v0040 v0041 v0042 v0043 v0044 v0045 v0046 v0047 v0048 v0049 v0050 v0051 v0052 v0053 v0054 v0055 v0056 v0057 v0058 v0059 v0060 v0061 v0062 v0063 v0064 v0065 v0066 v0067 v0068 v0069 v0070 v0071 v0072 v0073 v0074 v0075 v0076 v0077 v0078 v0079 v0080 v0081 v0082 v0083 v0084 v0085 v0086 v0087 v0088 v0089 v0090 v0091 v0092 v0093 v0094 v0095 v0096 v0097 v0098 v0099 v0100 v0101 v0102 v0103 v0104 v0105 v0106 v0107 v0108 v0109 v0110 v0111 v0112 v0113 v0114 -classifier sharkrf -classifier.sharkrf.nbtrees 500
'''

PANGAEA_classify('G:/DATA/MODIS','D:/TEST/MatoGrosso/ST/camara/RF_Full.model','D:/TEST/MatoGrosso/ST/camara/Maps')
PANGAEA_postproc('D:/TEST/MatoGrosso/ST/camara/Maps','D:/TEST/MatoGrosso/ST/camara/MatoGrossoMask.tif')
###################################



